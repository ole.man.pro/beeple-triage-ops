# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/remove_idle_labels_on_activity'
require_relative '../../triage/triage/event'

RSpec.describe Triage::RemoveIdleLabelOnActivity do
  include_context 'with event', 'Triage::NoteEvent' do
    let(:event_attrs) do
      {
        object_kind: 'note',
        noteable_type: 'merge_request',
        from_gitlab_org?: from_gitlab_org
      }
    end
    let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL, described_class::IDLE_LABEL] }
  end

  let(:from_gitlab_org) { true }

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.note']

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      let(:from_gitlab_org) { false }
      let(:labels) { [] }

      include_examples 'event is not applicable'
    end

    context 'when event project is under gitlab-org' do
      let(:from_gitlab_org) { true }

      context 'when there is no `community contribution` label' do
        let(:label_names) { [described_class::IDLE_LABEL] }

        include_examples 'event is not applicable'
      end

      context 'when there is a community label' do
        context 'but there is no idle or stale label' do
          let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL] }

          include_examples 'event is not applicable'
        end

        context 'and there is an idle label' do
          include_examples 'event is applicable'
        end

        context 'and there is a stale label' do
          let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL, described_class::STALE_LABEL] }

          include_examples 'event is applicable'
        end

        context 'and there is both an idle and stale label' do
          let(:label_names) { [described_class::COMMUNITY_CONTRIBUTION_LABEL, described_class::IDLE_LABEL, described_class::STALE_LABEL] }

          include_examples 'event is applicable'
        end
      end
    end
  end

  describe '#process' do
    it 'posts a message to remove the labels' do
      expected_message =<<~MARKDOWN.chomp
        /unlabel ~"#{described_class::IDLE_LABEL}" ~"#{described_class::STALE_LABEL}"
      MARKDOWN

      expect_comment_request(event: event, body: expected_message) do
        subject.process
      end
    end
  end
end
