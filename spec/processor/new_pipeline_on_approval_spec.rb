# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/new_pipeline_on_approval'
require_relative '../../triage/triage/event'

RSpec.describe Triage::NewPipelineOnApproval do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:approver_username) { 'approver' }
    let(:project_id) { Triage::Event::GITLAB_PROJECT_ID }
    let(:merge_request_iid) { 300 }
    let(:event_attrs) do
      {
        object_kind: 'merge_request',
        action: 'approval',
        from_gitlab_org?: true,
        from_gitlab_org_gitlab?: true,
        gitlab_org_author?: true,
        jihu_contributor?: false,
        source_branch_is?: false,
        target_branch_is_stable_branch?: false,
        event_user_username: approver_username,
        iid: merge_request_iid,
        project_id: project_id
      }
    end
  end

  subject { described_class.new(event) }

  include_examples 'registers listeners', ['merge_request.approval', 'merge_request.approved']

  let(:merge_request_notes) do
    [
      { "body": "review comment 1" },
      { "body": "review comment 2" }
    ]
  end

  let(:merge_request_changes) do
    {
      'changes' => [
        {
          "old_path" => "lib/gitlab.rb",
          "new_path" => "lib/gitlab.rb"
        }
      ]
    }
  end

  before do
    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/notes?per_page=100",
      response_body: merge_request_notes)

    stub_api_request(
      path: "/projects/#{project_id}/merge_requests/#{merge_request_iid}/changes",
      response_body: merge_request_changes)
  end

  describe '#applicable?' do
    context 'when event is not from gitlab-org/gitlab' do
      before do
        allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is not from gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request source branch is `release-tools/update-gitaly`' do
      before do
        allow(event).to receive(:source_branch_is?).with(described_class::UPDATE_GITALY_BRANCH).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request target branch is a stable branch' do
      before do
        allow(event).to receive(:target_branch_is_stable_branch?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains only qa and docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            }
          ]
        }
      end

      include_examples 'event is not applicable'
    end

    context 'when merge request contains a mixture of docs and non-docs changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "doc/user/index.md",
              "new_path" => "doc/user/index.md"
            },
          ]
        }
      end

      include_examples 'event is applicable'
    end

    context 'when merge request contains a mixture of qa and non-qa changes' do
      let(:merge_request_changes) do
        {
          'changes' => [
            {
              "old_path" => "lib/gitlab.rb",
              "new_path" => "lib/gitlab.rb"
            },
            {
              "old_path" => "qa/foo.rb",
              "new_path" => "qa/foo.rb"
            },
          ]
        }
      end

      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    context 'when merge request author is a gitlab-org member' do
      it 'triggers a new pipeline and posts a comment to inform that a new pipeline has been triggered' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. To ensure full test coverage, a new pipeline has been started.
          
          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        MARKDOWN

        expect(Triage::TriggerPipelineOnApprovalJob).to receive(:perform_async)
          .and_call_original

        expect_api_requests do |requests|
          requests << stub_api_request(verb: :post, path: "#{event.noteable_path}/pipelines")
          requests << stub_comment_request(event: event, body: body)

          subject.process

          Triage::TriggerPipelineOnApprovalJob.drain!
        end
      end
    end

    context 'when merge request author is not a gitlab-org member' do
      before do
        allow(event).to receive(:gitlab_org_author?).and_return(false)
      end

      it 'posts a discussion to nudge the approver to start a new pipeline' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. To ensure full test coverage, please start a new pipeline before merging.
          
          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end

      context 'and project is outside of gitlab-org/gitlab' do
        before do
          allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
        end

        include_examples 'event is not applicable'
      end
    end

    context 'when merge request author is a JiHu contributor' do
      before do
        allow(event).to receive(:gitlab_org_author?).and_return(false)
        allow(event).to receive(:jihu_contributor?).and_return(true)
      end

      it 'posts a discussion to nudge the approver to start a new pipeline' do
        body = <<~MARKDOWN.chomp
          #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
          :wave: `@#{approver_username}`, thanks for approving this merge request.

          This is the first time the merge request is approved. To ensure full test coverage, please start a new pipeline before merging, **and wait for AppSec approval**.

          cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions).

          For more info, please refer to the following links:
          - [`rspec` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-jobs)
          - [`jest` minimal jobs](https://docs.gitlab.com/ee/development/pipelines.html#jest-minimal-jobs)
          - [merging a merge request](https://docs.gitlab.com/ee/development/code_review.html#merging-a-merge-request).
        MARKDOWN

        expect_discussion_request(event: event, body: body) do
          subject.process
        end
      end

      context 'and project is outside of gitlab-org/gitlab' do
        before do
          allow(event).to receive(:from_gitlab_org_gitlab?).and_return(false)
        end

        it 'posts a discussion to request appsec review' do
          body = <<~MARKDOWN.chomp
            #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
            :wave: `@#{approver_username}`, thanks for approving this merge request.

            This is the first time the merge request is approved. Please wait for AppSec approval.

            cc @gitlab-com/gl-security/appsec this is a ~"JiHu contribution", please follow the [JiHu contribution review process](https://about.gitlab.com/handbook/ceo/chief-of-staff-team/jihu-support/jihu-security-review-process.html#security-review-workflow-for-jihu-contributions).
          MARKDOWN

          expect_discussion_request(event: event, body: body) do
            subject.process
          end
        end
      end
    end
  end
end
