# frozen_string_literal: true

require 'spec_helper'

require_relative '../../triage/processor/plan_ux_mr_review_support'
require_relative '../../triage/triage/event'

RSpec.describe Triage::PlanUxMrReviewSupport do
  include_context 'with event', 'Triage::MergeRequestEvent' do
    let(:event_attrs) do
      {
        from_gitlab_org?: true,
        project_id: project_id,
        iid: merge_request_iid,
        wip?: false
      }
    end
    let(:project_id) { 123 }
    let(:merge_request_iid) { 300 }
    let(:label_names) { [described_class::UX_LABEL, described_class::PLAN_LABEL] }
  end

  subject { described_class.new(event) }

  before do
    stub_api_request(
      path: "/projects/#{event.project_id}/#{event.object_kind}s/#{event.iid}/notes",
      query: { per_page: 100 },
      response_body: [])
  end

  include_examples 'registers listeners', ["merge_request.update", "merge_request.open"]

  describe '#applicable?' do
    context 'when event project is not under gitlab-org' do
      before do
        allow(event).to receive(:from_gitlab_org?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when resource is not opened' do
      before do
        allow(event).to receive(:resource_open?).and_return(false)
      end

      include_examples 'event is not applicable'
    end

    context 'when event is a Draft' do
      before do
        allow(event).to receive(:wip?).and_return(true)
      end

      include_examples 'event is not applicable'
    end

    context 'when other label is added' do
      let(:label_names) { ['bug'] }

      include_examples 'event is not applicable'
    end

    context 'when no label is added' do
      let(:label_names) { [] }

      include_examples 'event is not applicable'
    end

    context 'when UX and devops::plan labels are added' do
      include_examples 'event is applicable'
    end
  end

  describe '#process' do
    before do
      srand 1234
    end

    it 'posts a comment' do
      body = <<~MARKDOWN.chomp
        #{subject.__send__(:unique_comment).__send__(:hidden_comment)}
        Thanks for helping us improve the UX of GitLab, we have automatically assigned a UX reviewer for this merge request.
        /assign_reviewer #{described_class::REVIEWERS.sample}
      MARKDOWN

      expect_comment_request(event: event, body: body) do
        subject.process
      end
    end
  end
end
