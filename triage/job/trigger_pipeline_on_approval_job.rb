# frozen_string_literal: true

require_relative '../triage/job'
require_relative '../triage/reaction'
require_relative '../triage/sentry'

module Triage
  class TriggerPipelineOnApprovalJob < Job
    # Unfortunately, we cannot make on demand workers from sucker_punch:
    # https://github.com/brandonhilkert/sucker_punch/blob/v3.0.1/lib/sucker_punch/queue.rb#L23-L24
    # Consider make a feature request? Would be nice to set 2:10
    workers 5

    def perform(noteable_path, message)
      Triage.api_client.post("#{noteable_path}/pipelines")

      Reaction.add_comment(message, noteable_path)
    end
  end
end
