# frozen_string_literal: true

require_relative '../triage/error'
require_relative '../triage/event'
require_relative '../triage/handler'
require_relative '../triage/job'
require_relative '../triage/sentry'
require_relative '../triage'

module Triage
  class ProcessorJob < Job
    workers 16 # Puma has default concurrency set at min:0 max:16

    attr_reader :event

    def perform(payload)
      @event = Triage::Event.build(payload)

      logger.info("Performing job", klass: self.class, resource_url: event.url)

      handler = Handler.new(event)

      start_time = Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second)
      results = handler.process

      capture_and_log_errors(results)

      messages = results.transform_values(&:message).compact

      ::Rack::Response.new([JSON.dump(status: :ok, messages: messages)]).finish
    rescue Triage::ClientError => error
      log_error(error)
      error_response(error)
    rescue => error
      log_error(error)
      capture_error(error)
      error_response(error, status_code: 500)
    ensure
      log_request_processing(results, start_time) if results
    end

    private

    def log_request_processing(results, start_time)
      logger.info(
        'Event processing',
        dry_run: Triage.dry_run?,
        event_class: event.class,
        event_key: event.key,
        event_payload: event.payload,
        results: results_for_logs(results),
        duration: (Process.clock_gettime(Process::CLOCK_MONOTONIC, :float_second) - start_time).round(5)
      )
    end

    def results_for_logs(results)
      results.each_with_object([]) do |(processor, result), memo|
        memo << { processor: processor, **result.to_h }
      end
    end

    def error_response(error, status_code: 400)
      # We're always responding with HTTP 200 here, because otherwise
      # we might make ourselves disabled from getting more webhook events.
      # See: https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/921#note_875934104
      #
      # * If we give 500 error, we'll be temporarily disabled for 10 minutes
      #   as the initial backoff, and double to 20 minutes upon next
      #   consecutive 500 error.
      # * If we give 400 error 3 consecutive times, we'll be disabled
      #   permanently.
      actual_http_status = 200

      ::Rack::Response.new([JSON.dump(status: :error, status_code: status_code, error: error.class, message: error.message)], actual_http_status).finish
    end

    def capture_and_log_errors(results)
      results.each do |processor, result|
        next unless result.error

        log_error(result.error, processor)
        capture_error(result.error, processor)
      end
    end

    def capture_error(error, processor = nil)
      Raven.tags_context(object_kind: event.class.to_s, service: 'reactive')
      Raven.extra_context(processor: processor, payload: event.payload)
      Raven.capture_exception(error)
    end

    def log_error(error, processor = nil)
      logger.error(error, processor: processor)
    end
  end
end
