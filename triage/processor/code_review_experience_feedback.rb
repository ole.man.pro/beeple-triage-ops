# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'

module Triage
  class CodeReviewExperienceFeedback < Processor

    react_to 'merge_request.merge', 'merge_request.close'

    def applicable?
      (event.from_gitlab_org? && event.wider_community_author?) ||
        (event.from_gitlab_com? && event.wider_gitlab_com_community_author?)
    end

    def process
      post_review_experience_comment
    end

    private

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end

    def post_review_experience_comment
      add_comment(message.strip)
    end

    def message
      comment =  <<~MARKDOWN.chomp
        Hi @#{event.resource_author.username},

        We would love to know how you found your code review experience in this merge request! Please leave a :thumbsup: or a :thumbsdown: on this comment to describe your experience.

        Once done, please comment `@gitlab-bot feedback` below and feel free to leave any additional feedback you have in the same comment.

        You can also fill out a [5 minute survey](https://forms.gle/g26h8uEKgvTLGSQw6) to provided additional feedback on how GitLab can improve the contributor experience.

        Thanks for your help! :heart:
      MARKDOWN

      unique_comment.wrap(comment)
    end
  end
end
