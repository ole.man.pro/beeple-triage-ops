# frozen_string_literal: true

require_relative '../triage/processor'
require_relative '../triage/unique_comment'
require 'uri'

module Triage
  # Copies required labels from main issue to security implementation issue
  # for better discovery and triaging
  class CopySecurityIssueLabels < Processor
    react_to 'issue.open', 'issue.update'

    SECURITY_GITLAB_PROJECT_PATH = "#{GITLAB_ORG_SECURITY_GROUP}/gitlab"
    ISSUE_PATH_INFO_REGEXP = %r{/(?<project_path>.*)/-/issues/(?<iid>\d+).*}
    DESCRIPTION_MAIN_ISSUE_LINK_REGEXP = /Issue on \[GitLab\].*\|(?<url>.*)\|$/

    REQUIRED_LABELS = [
      'security', 'vulnerability', 'breaking change',
      'severity::*', 'type::*', 'group::*',
      'devops::*', 'section::*', 'backend', 'frontend'].freeze

    def self.required_labels
      @required_labels ||= REQUIRED_LABELS.map do |label_pattern|
        label_regexp = label_pattern.gsub('*','.*')
        /^#{label_regexp}$/
      end
    end

    def applicable?
      event.from_gitlab_org_security? &&
        security_implementation_issue? &&
        has_main_issue_linked? &&
        unique_comment.no_previous_comment? &&
        labels_to_add.any?
    end

    def process
      add_comment(comment_body)
    end

    private

    def security_implementation_issue?
      event.project_path_with_namespace == SECURITY_GITLAB_PROJECT_PATH
    end

    def has_main_issue_linked?
      main_issue_url_info
    end

    def main_issue_api_path
      return unless main_issue_url_info

      @main_issue_api_path ||= "/projects/#{CGI.escape(main_issue_url_info['project_path'])}/issues/#{main_issue_url_info['iid']}"
    end

    def main_issue
      return unless main_issue_api_path

      @main_issue ||= Triage.api_client.get(main_issue_api_path)
    rescue Gitlab::Error::NotFound
      nil
    end

    def main_issue_url_info
      @main_issue_url_info ||= begin
        data = event.description.match DESCRIPTION_MAIN_ISSUE_LINK_REGEXP
        return unless data

        uri = URI.parse(data[:url].strip)
        ISSUE_PATH_INFO_REGEXP.match(uri.path)&.named_captures
      rescue URI::InvalidURIError
        nil
      end
    end

    def labels_to_add
      @labels_to_add ||= begin
        missing_label_regexes = self.class.required_labels.reject do |required_label_regex|
          event.label_names.any? { |name| required_label_regex.match?(name) }
        end

        if missing_label_regexes.empty? || !main_issue
          []
        else
          missing_label_regexes.flat_map do |missing_label_regex|
            main_issue.labels.select { |main_issue_label| missing_label_regex.match?(main_issue_label)  }
          end
        end
      end
    end

    def comment_body
      markdown_labels_to_add = labels_to_add.map do |label_name|
        %Q(~"#{label_name}")
      end
      comment = <<~MARKDOWN.chomp
        Copied #{markdown_labels_to_add.join(' ')} from main project issue.
        /label #{markdown_labels_to_add.join(' ')}
      MARKDOWN

      unique_comment.wrap(comment).strip
    end

    def unique_comment
      @unique_comment ||= UniqueComment.new(self.class.name, event)
    end
  end
end
