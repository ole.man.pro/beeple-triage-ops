# frozen_string_literal: true

require_relative '../triage/sucker_punch'

module Triage
  class Job
    include SuckerPunch::Job

    def self.drain!
      pool = SuckerPunch::Queue::QUEUES.delete(to_s)

      return true unless pool

      # See https://github.com/brandonhilkert/sucker_punch/blob/v3.0.1/lib/sucker_punch/queue.rb#L74
      deadline = Time.now + SuckerPunch.shutdown_timeout

      pool.shutdown

      remaining = deadline - Time.now

      while remaining > SuckerPunch::Queue::PAUSE_TIME
        break if pool.shutdown?
        sleep SuckerPunch::Queue::PAUSE_TIME
        remaining = deadline - Time.now
      end

      remaining > 0
    end
  end
end
