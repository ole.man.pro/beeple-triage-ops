# frozen_string_literal: true

require_relative 'logging'
require_relative 'sentry'

require 'sucker_punch'

SuckerPunch.logger = Triage::Logging.sucker_punch_logger

SuckerPunch.exception_handler = -> (error, klass, args) do
  Raven.tags_context(process: :sucker_punch)
  Raven.extra_context(job_class: klass, job_args: args)
  Raven.capture_exception(error)

  SuckerPunch.logger.error(error, klass: klass, args: args)
end
