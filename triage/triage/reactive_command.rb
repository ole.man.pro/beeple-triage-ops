# frozen_string_literal: true

require_relative '../triage'

module Triage
  # Allow processors to react to commands.
  #
  # The concern shouldn't be included directly, but instead it can be enabled in a processor with
  #
  #   define_command name: 'command_name'
  #
  #   # A regex can be defined to detect command arguments
  #   define_command name: 'command_name', args_regex: /\w+/
  #
  # The `command.valid?(event)` method should be called in the `#applicable?` method of the processor.
  # The `command.args(event)` method can be used to access the arguments passed to the command.
  module ReactiveCommand
    COMMAND_PREFIX_REGEXP = /#{Regexp.escape(::Triage::GITLAB_BOT)}[[:space:]]+/

    def define_command(name:, args_regex: nil)
      define_method(:command) do
        @command ||= ReactiveCommand::Command.new(name: name, args_regex: args_regex)
      end
    end

    class Command
      attr_reader :name, :args_regex

      def initialize(name:, args_regex: nil)
        @name = name
        @args_regex = args_regex
      end

      def valid?(event)
        event.new_comment.match?(command_with_args_regex)
      end

      def args(event)
        if @args_regex
          matching_command_with_args(event)
            .sub(command_regexp, '')
            .scan(@args_regex)
            .flatten
            .compact
        else
          []
        end
      end

      private

      def command_regexp
        /^#{COMMAND_PREFIX_REGEXP}#{Regexp.escape(@name)}/
      end

      def command_with_args_regex
        /^#{command_regexp}[[:space:]]*.*$/
      end

      def matching_command_with_args(event)
        event.new_comment.match(command_with_args_regex)[0]
      end
    end
  end
end
