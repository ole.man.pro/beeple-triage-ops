# frozen_string_literal: true

require 'rack/requestid'

require_relative 'triage/job/keep_cache_warm_job'
require_relative 'triage/rack/attach_logger'
require_relative 'triage/rack/attach_request_id'
require_relative 'triage/rack/authenticator'
require_relative 'triage/rack/dashboard'
require_relative 'triage/rack/error_handler'
require_relative 'triage/rack/processor'
require_relative 'triage/rack/request_logger'
require_relative 'triage/rack/webhook_event'
require_relative 'triage/triage/logging'

# Warm the cache immediately
Triage::KeepCacheWarmJob.perform_async

use Rack::RequestID # makes sure `env` has an X-Request-Id header

use Triage::Rack::AttachLogger, Triage::Logging.rack_logger
use Triage::Rack::AttachRequestId # Replaces logger with a child logger that's tagged with the request ID
use Triage::Rack::RequestLogger # logs every request with timing data, request result, etc.

use Rack::ContentType, 'application/json'
use Triage::Rack::ErrorHandler

map '/dashboard' do
  use Rack::Auth::Basic do |*credentials|
    Triage::Rack::Authenticator.secure_compare(
      credentials.join,
      ENV['GITLAB_DASHBOARD_TOKEN'].to_s)
  end

  run Triage::Rack::Dashboard.new
end

use Triage::Rack::Authenticator
use Triage::Rack::WebhookEvent
run Triage::Rack::Processor.new
