# frozen_string_literal: true

require 'json'
require 'yaml'
require 'httparty'

class WwwGitLabCom
  WWW_GITLAB_COM_SECTIONS_JSON = 'https://about.gitlab.com/sections.json'
  WWW_GITLAB_COM_STAGES_JSON = 'https://about.gitlab.com/stages.json'
  WWW_GITLAB_COM_GROUPS_JSON = 'https://about.gitlab.com/groups.json'
  WWW_GITLAB_COM_CATEGORIES_JSON = 'https://about.gitlab.com/categories.json'
  WWW_GITLAB_COM_TEAM_YML = 'https://about.gitlab.com/company/team/team.yml'
  ROULETTE_JSON = 'https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json'

  def self.sections
    @sections ||= fetch_json(WWW_GITLAB_COM_SECTIONS_JSON)
  end

  def self.stages
    @stages ||= fetch_json(WWW_GITLAB_COM_STAGES_JSON)
  end

  def self.groups
    @groups ||= fetch_json(WWW_GITLAB_COM_GROUPS_JSON)
  end

  def self.categories
    @categories ||= fetch_json(WWW_GITLAB_COM_CATEGORIES_JSON)
  end

  def self.team_from_www
    @team_member_from_www ||=
      fetch_yml(WWW_GITLAB_COM_TEAM_YML).each_with_object({}) do |item, memo|
        memo.merge!({ item['gitlab'] => item })
      end
  end

  def self.roulette
    @roulette ||= fetch_json(ROULETTE_JSON)
  end

  def self.fetch_json(json_url)
    json = with_retries { HTTParty.get(json_url, format: :plain) }
    JSON.parse(json)
  end
  private_class_method :fetch_json

  def self.fetch_yml(yaml_url)
    YAML.load(HTTParty.get(yaml_url))
  end
  private_class_method :fetch_yml

  def self.with_retries(attempts: 3)
    yield
  rescue Errno::ECONNRESET, OpenSSL::SSL::SSLError, Net::OpenTimeout
    retry if (attempts -= 1) > 0
    raise
  end
  private_class_method :with_retries
end
